#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <nvs_flash.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "sdkconfig.h"
#include "dht11.h"

#define DEFAULT_VREF    1100        //Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES   64          //Multisampling
#define DHTPIN 5 //GPIO where is connected the DTH11
#define DHTTYPE DHT11

extern void app_main () { //Pongo esto as� porque tomo cosas de un archivo .cpp no .c
	DHT11_init(DHTPIN);

	while(1) {
		printf("Temperature: %d\n", DHT11_read().temperature);
		printf("Humidity: %d\n", DHT11_read().humidity);

		vTaskDelay(250); //delay 250 ms
	}

//	return 0;
}
